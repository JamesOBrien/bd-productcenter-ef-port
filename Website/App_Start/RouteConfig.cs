﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Website
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "ProductCenter",
                url: "ProductCenter",
                defaults: new { controller = "ProductCenter", action = "Index" }
            );

            routes.MapRoute(
                name: "ProductDetail",
                url: "ProductCenter/{productId}",
                defaults: new { controller = "ProductCenter", action = "ProductDetail" },
                constraints: new { productId = "^[0-9]+$" } //only numeric digits
            );

            routes.MapRoute(
                name: "ProductGroup",
                url: "ProductCenter/{groupId}",
                defaults: new { controller = "ProductCenter", action = "ProductGroup" }
                //constraints: new { groupId = "^[a-z,A-Z,-]+$" } //Alpha only
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );


        }
    }
}