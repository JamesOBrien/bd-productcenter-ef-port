﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Utilities;
using DS_SiteData;



namespace Website.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            BDProductCatalog Catalog = new BDProductCatalog();

            Catalog.Products = CSV.ReadCSVFile(@"C:\Users\James\Dropbox\BD Files\DS-Export\productCatalog\products.txt");
            Catalog.Inserts = CSV.ReadCSVFile(@"C:\Users\James\Dropbox\BD Files\DS-Export\productCatalog\inserts.txt");

            Catalog.Families = CSV.ReadCSVFile(@"C:\Users\James\Dropbox\BD Files\DS-Export\productCatalog\families.txt");
            Catalog.Lines = CSV.ReadCSVFile(@"C:\Users\James\Dropbox\BD Files\DS-Export\productCatalog\lines.txt");
            Catalog.Categories = CSV.ReadCSVFile(@"C:\Users\James\Dropbox\BD Files\DS-Export\productCatalog\categories.txt");

            return View(Catalog);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
