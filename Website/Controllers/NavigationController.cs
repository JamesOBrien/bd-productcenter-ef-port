﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using DS_SiteData;

namespace Website.Controllers
{
    public class LeftNavItem
    {
        public int Level { get; set; }
        public string Href { get; set; }
        public string DisplayText { get; set; }
    }

    public class NavigationController : Controller
    {
        //
        // GET: /Nav/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LeftNav(string CurrentLocation)
        {
            using (DS_SiteDataDbContext context = new DS_SiteDataDbContext())
            {
                List<LeftNavItem> NavItems = context
                    .ProductGroups.AsNoTracking()
                    .Where(x => x.WctKey.Contains("PF"))
                    .OrderBy(x => x.ListPosition)
                    .Select(x => new LeftNavItem{
                        Level = 0,
                        Href = "/ProductCenter/" + x.UrlKey,
                        DisplayText = x.Name
                    })
                    .ToList();

                return PartialView("_LeftNav", NavItems);
            }

        }

    }
}
