﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Utilities;
using DS_SiteData;



namespace Website.Controllers
{
    public class ProductCenterController : Controller
    {

        public ActionResult Index()
        {
            using(DS_SiteDataDbContext context = new DS_SiteDataDbContext()){
                List<ProductGroup> productGroups = context
                    .ProductGroups.AsNoTracking()
                    .Where(x=> x.WctKey.Contains("PF"))
                    .OrderBy(x=>x.ListPosition)
                    .ToList();

                ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
                return View(productGroups);
            }
        }


        public ActionResult ProductDetail(string productId)
        {

            using (DS_SiteDataDbContext context = new DS_SiteDataDbContext())
            {

                Product prodToDisplay = context.Products.AsNoTracking().Include("ProductInsert").Where(x => x.CatalogNumber == productId).First();

                ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
                return View(prodToDisplay);
            }
        }

        public ActionResult ProductGroup(string groupId)
        {

            using (DS_SiteDataDbContext context = new DS_SiteDataDbContext())
            {
                ProductGroup productGroup = context
                    .ProductGroups.AsNoTracking()
                    .Include("Children")
                    .Include("GroupProducts")
                    .Where(x => x.UrlKey == groupId)
                    .First();

                ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
                return View(productGroup);
            }

        }

    }
}
