﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DS_SiteData
{
    public class ProductGroup : Entity
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string WctKey { get; set; }
        public string Name { get; set; }
        public string RawName { get; set; }
        public float ListPosition { get; set; }
        public string LongDescription { get; set; }
        public string ShortDesciption { get; set; }
        public string UrlKey { get; set; }

        public string ParentUrlKey { get; set; }
        public string ParentWctKey { get; set; }

        [ForeignKey("ParentWctKey")]
        public virtual ProductGroup Parent { get; set; }
        public virtual ICollection<ProductGroup> Children { get; set; }

        public virtual ICollection<Product> GroupProducts { get; set; }
    }
}

//public class Category
//{
//    public int CategoryId { get; set; }
//    public string Name { get; set; }
//    public int? ParentId { get; set; }
//    public virtual Category Parent { get; set; }
//    public virtual ICollection<Category> Children { get; set; }
//}