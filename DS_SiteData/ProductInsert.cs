﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DS_SiteData
{
    public class ProductInsert : Entity
    {
        public string PartNumber { get; set; }
        public string FileName { get; set; }
        public string Type { get; set; }
        public string Summary { get; set; }
        public string Reagents { get; set; }
        public string Warnings { get; set; }
        public string QualityControl { get; set; }
        public string IntendedUse { get; set; }
    }
}
