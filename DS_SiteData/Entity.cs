﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DS_SiteData
{
    /// <summary>
    /// The layer supertype for all entity framework entities
    /// </summary>
    public abstract class Entity
    {
        /// <summary>
        /// Gets or sets the concurrency token.
        /// </summary>
        /// <value>
        /// The concurrency token.
        /// </value>
        public byte[] ConcurrencyToken { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        public int Id { get; set; }
    }
}
