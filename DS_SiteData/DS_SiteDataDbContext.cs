﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Reflection;    

using Utilities;
using System.IO;

namespace DS_SiteData
{

    public class DS_SiteDataDBInitializer : CreateDatabaseIfNotExists<DS_SiteDataDbContext>
    {
        protected override void Seed(DS_SiteDataDbContext context)
        {

            BDProductCatalog Catalog = new BDProductCatalog();

            Assembly YouAreHere = Assembly.GetExecutingAssembly();
            using (Stream stream = YouAreHere.GetManifestResourceStream("DS_SiteData.Resources.products.txt")){
                Catalog.Products = CSV.ReadCSVStream(stream);
            }
            using (Stream stream = YouAreHere.GetManifestResourceStream("DS_SiteData.Resources.families.txt")){
                Catalog.Families = CSV.ReadCSVStream(stream);
            }
            using (Stream stream = YouAreHere.GetManifestResourceStream("DS_SiteData.Resources.lines.txt")){
                Catalog.Lines = CSV.ReadCSVStream(stream);
            }
            using (Stream stream = YouAreHere.GetManifestResourceStream("DS_SiteData.Resources.categories.txt")){
                Catalog.Categories = CSV.ReadCSVStream(stream);
            }
            using (Stream stream = YouAreHere.GetManifestResourceStream("DS_SiteData.Resources.inserts.txt")){
                Catalog.Inserts = CSV.ReadCSVStream(stream);
            }



            //Populate ProductGroups Table with Product Families
            foreach (var f in Catalog.Families)
            {
                ProductGroup family = new ProductGroup
                {
                    WctKey = f["WctKey"],
                    Name = f["Name"],
                    RawName = f["RawName"],
                    ListPosition = float.Parse(f["ListPosition"]),
                    LongDescription = f["LongDescription"],
                    ParentUrlKey = f["ParentUrlKey"],
                    //ParentWctKey = f["ParentWctKey"],  //Product Family has no parent
                    ShortDesciption = f["ShortDesciption"],
                    UrlKey = f["UrlKey"]
                };
                context.ProductGroups.Add(family);

            }
            context.SaveChanges();


            //Populate ProductGroups Table with Product Families
            foreach (var f in Catalog.Lines)
            {
                ProductGroup Line = new ProductGroup
                {
                    WctKey = f["WctKey"],
                    Name = f["Name"],
                    RawName = f["RawName"],
                    ListPosition = float.Parse(f["ListPosition"]),
                    LongDescription = f["LongDescription"],
                    ParentUrlKey = f["ParentUrlKey"],
                    ParentWctKey = f["ParentWctKey"],  
                    ShortDesciption = f["ShortDesciption"],
                    UrlKey = f["UrlKey"]
                };
                context.ProductGroups.Add(Line);

                //Fix-up parent association of product for ProductLIne
                if(f["Products"].Length > 0)
                    foreach (var prodID in f["Products"].Split(','))
                    {
                        Dictionary<string, string> prod = Catalog.Products.Where(x => x["Catalog_No"] == prodID).First();
                        if (prod != null)
                            prod["Parent_WCT_Key"] = f["WctKey"];
                    }

            }
            context.SaveChanges();


            //Populate ProductGroups Table with Product Families
            foreach (var f in Catalog.Categories)
            {
                ProductGroup Category = new ProductGroup
                {
                    WctKey = f["WctKey"],
                    Name = f["Name"],
                    RawName = f["RawName"],
                    ListPosition = float.Parse(f["ListPosition"]),
                    LongDescription = f["LongDescription"],
                    ParentUrlKey = f["ParentUrlKey"],
                    ParentWctKey = f["ParentWctKey"],
                    ShortDesciption = f["ShortDesciption"],
                    UrlKey = f["UrlKey"]
                };
                context.ProductGroups.Add(Category);

                //Fix-up parent association of product for ProductCategory
                if (f["Products"].Length > 0)
                    foreach (var prodID in f["Products"].Split(','))
                    {
                        Dictionary<string, string> prod = Catalog.Products.Where(x => x["Catalog_No"] == prodID).First();
                        if(prod!=null)
                            prod["Parent_WCT_Key"] = f["WctKey"];
                    }


            }
            context.SaveChanges();

            //Populate products catalog
            foreach (var p in Catalog.Products)
            {
                var prod = new Product
                {
                    CatalogNumber = p["Catalog_No"],
                    Code = p["Code"],
                    Name = p["Name"],
                    RawName = p["Raw_Name"],
                    Description = p["Description"],
                    Brand = p["Brand"],
                    Quantity = p["Quantity"],
                    Unit = p["Unit"],
                    Concentration = p["Concentration"],
                    ParentWCTKey = p["Parent_WCT_Key"],
                    IsRoot = (p["IsRoot"] == "true"),
                    Discontinued = (p["Discontinued"] == "true")
                };

                context.Products.Add(prod);
            }
            context.SaveChanges();


            //Populate ProductInserts and attach to associated products(which have already been saved)
            foreach (var p in Catalog.Inserts)
            {
                var insert = new ProductInsert
                {
                    PartNumber = p["PartNumber"],
                    FileName = p["FileName"],
                    Type = p["Type"],
                    Summary = p["SummaryAndExplanation"],
                    Reagents = p["Reagents"],
                    QualityControl = p["QualityControl"],
                    IntendedUse = p["IntendedUse"],
                    Warnings = p["WarningsAndPrecautions"]
                };
                context.ProductInserts.Add(insert);
                context.SaveChanges();

                //Insert has been saved, get generated Id for insert and assign it to associated Populate
                foreach (var prodID in p["AssociatedCatalogNumbers"].Split(','))
                {
                    try
                    {
                        Product prodToChange = context.Products.Where(x => x.CatalogNumber == prodID).First();
                        prodToChange.ProductInsertId = insert.Id;
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        do
                        {
                            Console.WriteLine("{0}", ex.Message);
                            ex = ex.InnerException;
                        } while (ex != null);

                    }
                }
            }



            //All standards will
            base.Seed(context);
        }
    }

    public class DS_SiteDataDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductInsert> ProductInserts { get; set; }
        public DbSet<ProductGroup> ProductGroups { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductGroup>()
                .HasOptional(x => x.Parent);

        }        
    }
}
