﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DS_SiteData
{
    public class Product : Entity
    {
        public string CatalogNumber { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string RawName { get; set; }
        public string Description { get; set; }
        public string Brand { get; set; }
        public string Quantity { get; set; }
        public string Unit { get; set; }
        public string Concentration { get; set; }
        public bool IsRoot { get; set; }
        public bool Discontinued { get; set; }

        public int? ProductInsertId { get; set; }

        [ForeignKey("ProductInsertId")]
        public virtual ProductInsert ProductInsert { get; set; }

        public string ParentWCTKey { get; set; }

        [ForeignKey("ParentWCTKey")]
        public virtual ProductGroup Parent { get; set; }
    }
}
