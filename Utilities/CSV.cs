﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;



namespace Utilities
{

    public class BDProductCatalog
    {
        public List<Dictionary<string, string>> Products;
        public List<Dictionary<string, string>> Lines;
        public List<Dictionary<string, string>> Families;
        public List<Dictionary<string, string>> Categories;
        public List<Dictionary<string, string>> Inserts;
    }

    public class CSV
    {

        public static List<Dictionary<string, string>> ReadCSVStream(Stream input)
        {

            List<Dictionary<string, string>> records = new List<Dictionary<string, string>>();

            using (TextFieldParser parser = new TextFieldParser(input) )
            {

                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters("|");
                parser.HasFieldsEnclosedInQuotes = false;


                if (!parser.EndOfData)
                {
                    string[] fieldNames = parser.ReadFields();
                    while (!parser.EndOfData)
                    {
                        //Processing row
                        string[] fields = parser.ReadFields();
                        Dictionary<string, string> row = new Dictionary<string, string>();
                        int column = 0;
                        foreach (string field in fields)
                        {
                            row.Add(fieldNames[column++], field);
                        }
                        records.Add(row);
                    }
                }
            }

            return records;

        }

        
        public static List<Dictionary<string, string>> ReadCSVFile(string FileName)
        {

            List<Dictionary<string, string>> records = new List<Dictionary<string, string>>();

            using (TextFieldParser parser = new TextFieldParser(FileName))
            {

                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters("|");
                parser.HasFieldsEnclosedInQuotes = false;


                if (!parser.EndOfData)
                {
                    string[] fieldNames = parser.ReadFields();
                    while (!parser.EndOfData)
                    {
                        //Processing row
                        string[] fields = parser.ReadFields();
                        Dictionary<string, string> row = new Dictionary<string, string>();
                        int column = 0;
                        foreach (string field in fields)
                        {
                            row.Add(fieldNames[column++], field);
                        }
                        records.Add(row);
                    }
                }
            }

            return records;

        }

    }
}
